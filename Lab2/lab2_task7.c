#include <stdio.h>
// таблица встречаемости
int main()
{	
	char ch;
	char frequency[255];
	short flag;
	for (flag = 0; flag < 255; flag++) frequency[flag] = 0;
	printf("Enter your line:\n");
	
	do
	{
		ch = getchar();
		frequency[ch]++;
				
	}
	while (ch!= 10);
	frequency[10] = 0;
	for (flag = 0; flag < 255; flag++)
		if (frequency[flag] != 0) printf ("[ '%c' ] || %d times\n", (char) flag, frequency[flag]);
	getchar();
	return 0;
}