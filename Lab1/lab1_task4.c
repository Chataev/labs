#include <stdio.h>

int main() {

	short foot,inch;
	float cm;

	printf("Enter length in 'American system'\n");
	scanf("%d%d", &foot, &inch);
	cm = foot*12*2.54 + inch*2.54;
	printf ("Your %d foot and %d inches equals to %.1f centimeters", foot, inch, cm);
	return 0;
}