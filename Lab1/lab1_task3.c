#include <stdio.h>

int main() {

	float value1,value2;
	char type;
	printf("Enter your value in Radians or Degrees\n");
	scanf("%f%c", &value1, &type);
	if (type == 'R' || type == 'r') {
			value2 = value1*57.3;
			printf ("%.2f%c = %.2fD \n", value1, type, value2);
	}
	if (type == 'D' || type == 'd') {
			value2 = value1*0.017;
			printf ("%.2f%c = %.4fR \n", value1, type, value2);
	}
	return 0;
}