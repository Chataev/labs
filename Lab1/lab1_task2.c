#include <stdio.h>

int main() {

	short hh = 0,mm = 0,ss = 0, flag = 1;
	printf("Enter your current time in correct form 'HH:MM:SS' (24h format)\n");

	do
	{
		scanf("%d:%d:%d",&hh,&mm,&ss);
		if (hh>24 || hh<0 || mm>60 || mm<0 || ss> 60 || ss< 0)
		{
			printf("Wrong type of Time, please enter Time in correct form 'HH:MM:SS'!\n");
		}
		else flag = 0;
	}
	while (flag);
	if (hh >= 5 && hh < 11) printf("Good morning!\n");
	else if (hh >= 11 && hh < 18) printf("Good day!\n");
		else if (hh >= 18 && hh < 22) printf("Good evening!\n");
		else printf("Good night!\n");

	return 0;
}