#include <stdio.h>

int main()
{	
	char ch;
	short word_count = 0, flag = 1;
	printf("Enter your line:\n");
	
	do
	{
		ch = getchar();
		if (ch == 10) break;
		if ( ch != ' ' && flag == 1 ) 
			{
				flag = 0;
				word_count++;
			}
		else if (ch == ' ' && flag == 0 )
			{
				flag = 1;
			}

	}
	while (1);

	printf ("Word count = %d", word_count);
	getchar();
	return 0;
}