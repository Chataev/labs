#include <stdio.h>

int main()
{	
	char ch;
	short word_count = 0, flag = 0;
	printf("Enter your line:\n");
	
	do
	{
		ch = getchar();
		if ( ch != ' ' && flag == 0) 
			{
				flag = 1;
			}
		else if (ch == ' ' && flag == 1 )
			{
				flag = 0;
				word_count++;
			}
	}
	while (ch!= 10);

	printf ("Word count = %d", word_count);
	getchar();
	return 0;
}